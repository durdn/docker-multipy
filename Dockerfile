FROM debian:jessie

RUN apt-get update && \
    apt-get install -y file curl git xz-utils build-essential \
                       libssl-dev libffi-dev libxml2-dev libxslt1-dev libsqlite3-dev && \
    apt-get clean

# Install Python 2.7, 3.4 and 3.3
RUN for v in "2.7.10" "3.4.3" "3.3.6"; do \
        cd /tmp/ && \
        curl -O https://www.python.org/ftp/python/${v}/Python-${v}.tar.xz && \
        tar xf Python-${v}.tar.xz && \
        cd Python-${v} && \
        ./configure --with-ensurepip && \
        make -j altinstall && \
        cd /tmp && \
        rm -rf Python-*; \
    done ;

# Install PyPy 2 & 3. These are subtly different so run in separate
# commands.
RUN cd /opt/ && \
    curl -L -O http://cobra.cs.uni-duesseldorf.de/~buildmaster/mirror/pypy-2.6.0-linux64.tar.bz2 && \
    tar xf pypy-2.6.0-linux64.tar.bz2 && \
    ln -s /opt/pypy-2.6.0-linux64/bin/pypy /usr/local/bin/ && \
    rm /opt/pypy-2.6.0-linux64.tar.bz2

RUN cd /opt/ && \
    curl -L -O https://bitbucket.org/pypy/pypy/downloads/pypy3-2.4.0-linux64.tar.bz2 && \
    tar xf pypy3-2.4.0-linux64.tar.bz2 && \
    ln -s /opt/pypy3-2.4.0-linux64/bin/pypy* /usr/local/bin/ && \
    rm /opt/pypy3-2.4.0-linux64.tar.bz2

# Set default python
RUN update-alternatives --install /usr/bin/python python /usr/local/bin/python2.7 10 && \
    update-alternatives --install /usr/bin/pip pip /usr/local/bin/pip2.7 10


# Add tox
RUN pip install tox
